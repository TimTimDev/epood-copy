<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-black">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-md mt-8 px-6 py-4 bg-red-400 shadow-2xl overflow-hidden sm:rounded">
        {{ $slot }}
    </div>
</div>
