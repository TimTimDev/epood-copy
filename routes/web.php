<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function() {
    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');
    //product
    Route::get('/products', [ProductController::class, 'index'])->name('product.index');
    Route::get('/product-add', [ProductController::class, 'create'])->name('product.add');
    Route::post('/product-submit', [ProductController::class, 'store'])->name('product.submit'); 
    

    //category
    Route::get('/category-add', [CategoryController::class, 'create'])->name('category.add');
    Route::post('/category-submit', [CategoryController::class, 'store'])->name('category.submit');
    
});

require __DIR__.'/auth.php';
